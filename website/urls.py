from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.utils.translation import gettext_lazy as _

from website.apps.flatpages_ckeditor import views

urlpatterns = (
    [
        # django-related options
        path("admin/", admin.site.urls),
        path("i18n/", include("django.conf.urls.i18n")),
    ]
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)  # noqa
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)  # noqa
)

urlpatterns += i18n_patterns(
    path(_("about/"), views.flatpage, {"url": "/about/"}, name="about"),
    path("", include("website.apps.gallery.urls")),
)
