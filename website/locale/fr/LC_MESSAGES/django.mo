��          �      ,      �  �  �  0        P     V     v     |     �     �     �  	   �     �     �     �     �     �     �  A  �  �    1   �  	   �  *   �          "     *  	   2  	   <     F  	   O     Y     f     o     |  	   �                                                   	      
                     
          <p>
            %(site_name)s, a <a href="https://gitlab.com/sodimel/django-simple-gallery">django-simple-gallery</a> instance.
          </p>
          <p>
            <i>
              Powered by <a href="https://docs.djangoproject.com/">Django</a>, <a href="https://simplecss.org/">Simple.css</a> and <a href="https://photoswipe.com/">PhotoSwipe</a>.
            </i>
          </p>
         
    Page %(current_page)s of %(total_pages)s
   About Add gallery from multiple files Admin Apply English First French Galleries Last Logout Next Previous Upload multiple files about/ Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
          <p>
            %(site_name)s, une instance de <a href="https://gitlab.com/sodimel/django-simple-gallery">django-simple-gallery</a>.
          </p>
          <p>
            <i>
              Tourne avec <a href="https://docs.djangoproject.com/">Django</a>, <a href="https://simplecss.org/">Simple.css</a> et <a href="https://photoswipe.com/">PhotoSwipe</a>.
            </i>
          </p>
         
    Page %(current_page)s sur %(total_pages)s
   À propos Créer une galerie avec plusieurs fichiers Admin Changer Anglais Première Français Galeries Dernière Déconnexion Suivante Précédente Héberger plusieurs fichiers a-propos/ 