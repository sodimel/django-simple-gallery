from .base import *  # noqa

SECRET_KEY = "django-insecure-!*652volee^im^%_&2yl6qe$-1*esn3*as(cuixna8q^g#f@ng"

DEBUG = True

INTERNAL_IPS = ALLOWED_HOSTS = ["127.0.0.1", "localhost"]

STATIC_URL = "/static/"

SECURE_SSL_REDIRECT = False
