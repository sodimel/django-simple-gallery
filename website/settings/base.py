from os import getenv
from pathlib import Path

from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


SECRET_KEY = getenv("DJANGO_SIMPLE_GALLERY_SECRET_KEY", None)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = [getenv("DJANGO_SIMPLE_GALLERY_ALLOWED_HOSTS", None)]

# Allow more fields/files to be posted (if we have 150 images (at ~5 fields) then it's over 1000 files and it's still fine)
DATA_UPLOAD_MAX_NUMBER_FIELDS = 10000
DATA_UPLOAD_MAX_NUMBER_FILES = 10000

# Application definition

INSTALLED_APPS = [
    "website.apps.gallery",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.flatpages",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.humanize",
    "django.contrib.sites",
    "ckeditor",
    "sorl.thumbnail",
    "parler",
    "adminsortable2",
    "website.apps.flatpages_ckeditor",
    "website",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.sites.middleware.CurrentSiteMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "website.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            BASE_DIR / "templates",
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

TEMPLATE_LOADERS = ("django.template.loaders.app_directories.load_template_source",)

WSGI_APPLICATION = "website.wsgi.application"

SITE_ID = 1


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"

# security stuff

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
SECURE_SSL_REDIRECT = True
SECURE_HSTS_SECONDS = 31536000
SECURE_HSTS_PRELOAD = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_REFERRER_POLICY = "strict-origin-when-cross-origin"
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True

# languages

LANGUAGES = [
    ("en", _("English")),
    ("fr", _("French")),
]

LANGUAGE_CODE = "en"

USE_L10N = True

USE_L10N = True

USE_THOUSAND_SEPARATOR = True

TIME_ZONE = getenv("DJANGO_SIMPLE_GALLERY_TIME_ZONE", "UTC")

USE_TZ = True


# django-parler config

PARLER_LANGUAGES = {
    1: (
        {
            "code": "en",
        },
        {
            "code": "fr",
        },
    ),
    "default": {
        "fallbacks": ["fr", "en"],
        "hide_untranslated": False,
    },
}

PARLER_DEFAULT_LANGUAGE_CODE = "en"


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_ROOT = getenv("DJANGO_SIMPLE_GALLERY_STATIC_ROOT", "static")

STATIC_URL = "/static/"

MEDIA_ROOT = getenv("DJANGO_SIMPLE_GALLERY_MEDIA_ROOT", "media")

MEDIA_URL = "/media/"

STATICFILES_DIRS = [
    BASE_DIR / getenv("DJANGO_SIMPLE_GALLERY_STATICFILES_DIR", "static"),
]

SESSION_ENGINE = "django.contrib.sessions.backends.signed_cookies"


# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"


# Ckeditor settings

CKEDITOR_BASEPATH = f"{STATIC_URL}ckeditor/ckeditor/"


# images/thumbnails settings

DJANGORESIZED_DEFAULT_SIZE = [
    int(getenv("DJANGO_SIMPLE_GALLERY_RESIZE_X", 2500)),
    int(getenv("DJANGO_SIMPLE_GALLERY_RESIZE_Y", 2500)),
]  # used to keep the ratio!
DJANGORESIZED_DEFAULT_QUALITY = int(
    getenv("DJANGO_SIMPLE_GALLERY_QUALITY", 75)
)  # webp lossless quality of ~50% is not really noticeable from webp lossless quality of 70-80% so why not?
DJANGORESIZED_DEFAULT_KEEP_META = True
DJANGORESIZED_DEFAULT_FORCE_FORMAT = "WEBP"  # YES PLEASE

THUMBNAIL_PREFIX = "thumbnails"
THUMBNAIL_QUALITY = int(getenv("DJANGO_SIMPLE_GALLERY_THUMB_QUALITY", 75))
THUMBNAIL_ORIENTATION = False

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.memcached.PyMemcacheCache",
        "LOCATION": "127.0.0.1:11211",
    }
}
