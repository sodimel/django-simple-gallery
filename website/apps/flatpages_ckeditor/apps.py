from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class GalleryConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "website.apps.flatpages_ckeditor"
    verbose_name = _("Flatpage")
    verbose_name_plural = _("Flatpages")
