from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from django.contrib.flatpages.models import FlatPage
from django.db import models
from parler.admin import TranslatableAdmin

from .models import FlatPageCkeditor


@admin.register(FlatPageCkeditor)
class FlatPageCkeditorAdmin(TranslatableAdmin):
    TranslatableAdmin.formfield_overrides = {
        models.TextField: {"widget": CKEditorWidget},
    }


admin.site.unregister(FlatPage)
