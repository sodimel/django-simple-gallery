��          �      �       H  w   I     �  	   �  G   �                (  	   8  
   B     M     c     i     w  A  }  y   �     9     ?  W   E     �     �     �     �     �     �     �     �     �        
                                   	                      Example: “flatpages/contact_page.html”. If this isn’t provided, the system will use “flatpages/default.html”. Flatpage Flatpages If this is checked, only logged-in users will be able to view the page. URL content enable comments flat page flat pages registration required sites template name title Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Exemple: “flatpages/contact_page.html”. si ce n'est pas rempli, le système va utiliser “flatpages/default.html”. Pages Pages Si cette case est cochée, seuls les utilisateurs connectés vont pouvoir voir la page. URL contenu activer les commentaires page page connexion requise sites nom du template titre 