from django.urls import path

from .views import GalleryView, ListGalleriesView

urlpatterns = [
    path("", ListGalleriesView.as_view(), name="galleries"),
    path("<slug:slug>/", GalleryView.as_view(), name="gallery_detail"),
]
