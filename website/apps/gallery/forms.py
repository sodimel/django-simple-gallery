from django import forms


class GalleryPasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput())
