from adminsortable2.admin import SortableAdminBase, SortableStackedInline
from django.contrib import admin, messages
from django.core.exceptions import PermissionDenied
from django.forms import ClearableFileInput, FileField, Form
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import path, reverse, reverse_lazy
from django.utils.crypto import get_random_string
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _
from parler.admin import TranslatableAdmin, TranslatableStackedInline

from .models import Gallery, Image, License


class ImageInline(
    SortableStackedInline, TranslatableStackedInline, admin.StackedInline
):
    model = Image


@admin.register(Gallery)
class GalleryAdmin(SortableAdminBase, TranslatableAdmin):
    date_hierarchy = "date_added"
    list_display = (
        "title",
        "added_by",
        "date_added",
        "date_edit",
        "get_nb_images",
        "is_published",
        "have_a_hash_protection",
        "get_url_hash",
        "is_password_protected",
        "password",
    )
    list_filter = ("added_by",)
    list_editable = (
        "is_published",
        "have_a_hash_protection",
        "is_password_protected",
        "password",
    )

    inlines = [
        ImageInline,
    ]

    def get_nb_images(self, obj):
        return str(obj.image_set.count())

    get_nb_images.short_description = _("Images number")

    def get_url_hash(self, obj):
        if obj.have_a_hash_protection and obj.url_hash:
            gallery_url = reverse_lazy("gallery_detail", args=(obj.slug,))
            return format_html(
                f"<a href='{gallery_url}?{obj.url_hash}'>{obj.url_hash}</a>"
            )
        else:
            return ""

    get_url_hash.short_description = _("URL hash")

    readonly_fields = ("url_hash", "date_added", "date_edit", "added_by")

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "is_published",
                    "title",
                    "slug",
                    "short_description",
                    "content",
                    "license",
                ),
            },
        ),
        (
            _("Config"),
            {
                "fields": (
                    (
                        "have_a_hash_protection",
                        "url_hash",
                    ),
                    "is_password_protected",
                    "password",
                )
            },
        ),
        (_("Infos"), {"fields": (("date_added", "date_edit", "added_by"),)}),
    )

    def save_model(self, request, obj, form, change):
        if not obj.id:
            obj.added_by = request.user

        if obj.have_a_hash_protection and obj.url_hash is None:
            obj.url_hash = get_random_string(length=32)

        if obj.is_password_protected and (obj.password is None or obj.password == ""):
            messages.add_message(
                request,
                messages.ERROR,
                _(
                    'You can\'t protect a gallery with a password... without the password! We unchecked the "gallery is password-protected" checkbox.'
                ),
            )
            obj.is_password_protected = False
        else:
            super(GalleryAdmin, self).save_model(request, obj, form, change)

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path("create/", self.upload_multiple_files, name="upload_multiple_files"),
        ]
        return my_urls + urls

    def upload_multiple_files(self, request):
        if not request.user.has_perm("gallery.add"):
            raise PermissionDenied()
        if request.method == "POST":
            form = FilesFieldForm(request.POST, request.FILES)
            if form.is_valid():
                gallery = Gallery()
                gallery.added_by = request.user
                gallery.save()
                for file in form.cleaned_data.get("multiple_files"):
                    image = Image()
                    image.image = file
                    image.gallery = gallery
                    image.save()
                return HttpResponseRedirect(
                    reverse(
                        "admin:%s_%s_change"
                        % (self.model._meta.app_label, self.model._meta.model_name),
                        args=(gallery.id,),
                    )
                )
        else:
            template = loader.get_template("gallery/upload_multiple_files.html")
            context = {
                "form": FilesFieldForm(),
            }
            return HttpResponse(template.render(context, request))


class MultipleFileInput(ClearableFileInput):
    allow_multiple_selected = True


class MultipleFileField(FileField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("widget", MultipleFileInput())
        super().__init__(*args, **kwargs)

    def clean(self, data, initial=None):
        single_file_clean = super().clean
        if isinstance(data, (list, tuple)):
            result = [single_file_clean(d, initial) for d in data]
        else:
            result = [single_file_clean(data, initial)]
        return result


class FilesFieldForm(Form):
    multiple_files = MultipleFileField()


@admin.register(License)
class LicenseAdmin(TranslatableAdmin):
    ...
