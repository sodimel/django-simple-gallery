from django.http import Http404
from django.views.generic import DetailView, ListView
from parler.views import TranslatableSlugMixin

from .forms import GalleryPasswordForm
from .models import Gallery


class ListGalleriesView(ListView):
    model = Gallery
    paginate_by = 5
    context_object_name = "galleries"
    queryset = Gallery.objects.filter(
        is_published=True, have_a_hash_protection=False
    ).order_by("-date_added")


class GalleryView(TranslatableSlugMixin, DetailView):
    model = Gallery
    context_object_name = "gallery"

    def post(self, request, *args, **kwargs):
        password_form = GalleryPasswordForm(self.request.POST or None)
        self.object = self.get_object()
        context = super().get_context_data(**kwargs)

        if password_form.is_valid():
            try:
                session_gallery_slug = f"gallery_{self.object.slug}"
                if password_form.cleaned_data["password"] == self.object.password:
                    request.session[session_gallery_slug] = True
                    context["session_gallery_valid"] = True
            except Gallery.DoesNotExist:
                return "nope"
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if not self.object.is_published:
            raise Http404()

        # has url hash?
        context["is_protected_by_hash"] = False
        context["url_has_a_hash"] = False
        if self.object.have_a_hash_protection:
            context["is_protected_by_hash"] = True
            for get_hash in self.request.GET.keys():
                if get_hash == self.object.url_hash:
                    context["url_has_a_hash"] = True

        # has password?
        if self.object.is_password_protected:
            context["password_form"] = GalleryPasswordForm()
            session_gallery_slug = f"gallery_{self.object.slug}"
            if session_gallery_slug not in self.request.session:
                self.request.session[session_gallery_slug] = False
            context["session_gallery_valid"] = self.request.session[
                session_gallery_slug
            ]

        return context
