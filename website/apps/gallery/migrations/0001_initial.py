# Generated by Django 3.2.16 on 2023-02-02 21:09

import ckeditor.fields
import django.db.models.deletion
import django_resized.forms
import parler.fields
import parler.models
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="Gallery",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "date_added",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="Date d'ajout"
                    ),
                ),
                (
                    "date_edit",
                    models.DateTimeField(
                        editable=False,
                        null=True,
                        verbose_name="Date de dernière édition",
                    ),
                ),
                (
                    "added_by",
                    models.ForeignKey(
                        blank=True,
                        editable=False,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to=settings.AUTH_USER_MODEL,
                        verbose_name="Auteur",
                    ),
                ),
            ],
            options={
                "verbose_name": "Galerie",
                "verbose_name_plural": "Galeries",
            },
            bases=(parler.models.TranslatableModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name="Image",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "image",
                    django_resized.forms.ResizedImageField(
                        crop=None,
                        force_format="WEBP",
                        keep_meta=True,
                        quality=50,
                        scale=None,
                        size=[1600, 1600],
                        upload_to="galleries/",
                        verbose_name="Image",
                    ),
                ),
                (
                    "gallery",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="gallery.gallery",
                        verbose_name="Galerie",
                    ),
                ),
            ],
            options={
                "verbose_name": "Image",
                "verbose_name_plural": "Images",
            },
            bases=(parler.models.TranslatableModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name="ImageTranslation",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "language_code",
                    models.CharField(
                        db_index=True, max_length=15, verbose_name="Language"
                    ),
                ),
                (
                    "short_description",
                    models.CharField(
                        blank=True,
                        help_text="Petite description qui sera affichée sous l'image.",
                        max_length=1024,
                        null=True,
                        verbose_name="Description courte de l'image",
                    ),
                ),
                (
                    "master",
                    parler.fields.TranslationsForeignKey(
                        editable=False,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="translations",
                        to="gallery.image",
                    ),
                ),
            ],
            options={
                "verbose_name": "Image Translation",
                "db_table": "gallery_image_translation",
                "db_tablespace": "",
                "managed": True,
                "default_permissions": (),
                "unique_together": {("language_code", "master")},
            },
            bases=(parler.models.TranslatedFieldsModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name="GalleryTranslation",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "language_code",
                    models.CharField(
                        db_index=True, max_length=15, verbose_name="Language"
                    ),
                ),
                (
                    "title",
                    models.CharField(
                        help_text="Le titre de votre galerie.",
                        max_length=1024,
                        verbose_name="Titre",
                    ),
                ),
                (
                    "slug",
                    models.SlugField(
                        blank=True,
                        help_text="Le slug est autogénéré lors de l'enregistrement de la galerie. Vous n'avez pas besoin de le remplir. Vous pouvez cependant le modifier s'il ne vous plaît pas.",
                        max_length=1024,
                        unique=True,
                        verbose_name="Slug",
                    ),
                ),
                (
                    "short_description",
                    models.CharField(
                        help_text="Petite description qui sera affichée dans la liste des galeries.",
                        max_length=1024,
                        verbose_name="Description courte",
                    ),
                ),
                (
                    "content",
                    ckeditor.fields.RichTextField(
                        help_text="Decription de la galerie.", verbose_name="Contenu"
                    ),
                ),
                (
                    "master",
                    parler.fields.TranslationsForeignKey(
                        editable=False,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="translations",
                        to="gallery.gallery",
                    ),
                ),
            ],
            options={
                "verbose_name": "Galerie Translation",
                "db_table": "gallery_gallery_translation",
                "db_tablespace": "",
                "managed": True,
                "default_permissions": (),
                "unique_together": {("language_code", "master")},
            },
            bases=(parler.models.TranslatedFieldsModelMixin, models.Model),
        ),
    ]
