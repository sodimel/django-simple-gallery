
# Django Simple Gallery

<div align="center">
  <img src="icon.webp" alt="django simple gallery logo" />
  <p>Small & fast gallery website powered by <a href="https://www.djangoproject.com/">Django</a>, <a href="https://simplecss.org/">SimpleCSS</a> & <a href="https://photoswipe.com/">Photoswipe</a>.</p>
</div>

----

## Screenshots

| ![powerful admin screenshot](https://gitlab.com/sodimel/django-simple-gallery/uploads/2a698fedfe055bed3091fb4353c56571/image.png) | ![host whole gallery using a specific form screenshot](https://gitlab.com/-/project/43075504/uploads/4483d0482e818cbe39be3fecc01c4478/image.png) | ![simple gallery list screenshot](https://gitlab.com/sodimel/django-simple-gallery/uploads/327b544c5e56df77417cf26e4f80b617/image.png) | ![nice gallery screenshot](https://gitlab.com/sodimel/django-simple-gallery/uploads/b6a7f2a31dcd85e3bc67d514549167c1/photoswipe.gif) |
| ---- | ---- | ---- | ---- |
| Powerful Django admin. | Host whole gallery using a specific form. | Simple gallery list. | Nice gallery view. |

----

## Features

- Create galleries from the Django admin, then publish them.
  - Upload multiple files one time, access your newly created gallery!
  - Create public galleries, they are present on the (public) gallery list.
  - Create private galleries, they are not on the public list and are only accessible using the random-generated hash in their URL available from the admin.
  - Create password-protected galleries, they are on the (public) gallery list (and their short description is displayed), but accessing them require a password that you can define in the admin.
  - (*Create private + password-protected galleries, unlisted **and** require a password!*)
- List galleries.
- Access specific galleries.
- Set license to individual images or whole galleries.
- Add an about page using flatpages (that are using a ckeditor widget instead of a plain ol' textarea)!

----

## Good to know

- If you want to use the multiple upload files feature? You'll need django>=5 in order to do this.

----

## Install

### Dev (test on your machine)

```bash
git clone https://gitlab.com/sodimel/django-simple-gallery.git
cd django-simple-gallery
sudo apt install pymemcache  # thumbnails requirements
python3 -m venv .venv
. .venv/bin/activate
echo "export DJANGO_SETTINGS_MODULE=website.settings.dev" >> .env  # you'll need more env vars, see below
source .env
python3 -m pip install -r requirements_dev.txt
python3 manage.py migrate
python3 manage.py ruserver 0.0.0.0:8000
```

### Deploy

You can [read this guide](https://gitlab.com/sodimel/share-links/-/blob/main/DEPLOY.md) that I created for another Django website and adapt it to this project.

----

## Env vars

* `DJANGO_SIMPLE_GALLERY_SECRET_KEY`: Secret key!
  * *Use this command to get a cool secret key directly from django:*
    ```sh
    python3 -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'
    ```
* `DJANGO_SIMPLE_GALLERY_ALLOWED_HOSTS`: URL of the website!
* `DJANGO_SIMPLE_GALLERY_STATIC_ROOT`: Static folder!
* `DJANGO_SIMPLE_GALLERY_MEDIA_ROOT`: Media folder!
* `DJANGO_SIMPLE_GALLERY_STATICFILES_DIR`: Name of the staticfiles dir in the URL (default: `static`).
* `DJANGO_SIMPLE_GALLERY_TIME_ZONE`: Set the time zone for edit dates, [see here](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones#PARIS) for a list (default `UTC`).
* `DJANGO_SIMPLE_GALLERY_RESIZE_X`: Max width of the uploaded images (default = `2500`px).
* `DJANGO_SIMPLE_GALLERY_RESIZE_Y`: Max height of the uploaded images (default = `2500`px).
* `DJANGO_SIMPLE_GALLERY_QUALITY`: Quality of the uploaded images (default = `75`%).
* `DJANGO_SIMPLE_GALLERY_THUMB_QUALITY`: Quality of the thumbnails (default = `75`%).

----

## How to add an about page?

* Create `.env`.
* `migrate`
* `createsuperuser`
* Go on the sites url `/admin`, login.
* Go to "flat pages ckeditor", click "add".
* Set `url` to `/about/` (that's the only required thing).
* Fill the rest of the form.
* You dit it!

----

## Todo

- [x] limit max nb of thumbnails in preview gallery
- [x] add licence to galleries & photos
- [x] filters in admin
- [x] favicon
- [x] doc/better readme
- [x] private galleries (unlisted, share by link only)?
- [ ] randomize thumbnails if more than a threshold?
- [ ] add an env var/a setting with max nb of thumbnails
- [x] document all env vars
- [ ] deploy guide
- [ ] rss
- [ ] sitemap
- [ ] rename images in admin
- [ ] preview images (thumbnails) in admin
- [ ] reorder images in admin
- [ ] upload a folder in the admin that creates a new gallery
- [ ] add optional period (start/end) of event in gallery
- [ ] filter thumbnails view (map ? period of event ? number of images ? search by title ?)
